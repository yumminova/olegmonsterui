define(function(require) {
	var $ = require('jquery'),
		_ = require('underscore'),
		monster = require('monster');

	var app = {
		name: 'olegmonsterui',

		css: [ 'app' ],

		i18n: { 
			'en-US': { customCss: false }
		},
		
		// Defines API requests not included in the SDK
		requests: {
			'olegmonsterui.devices.list': {
				url: 'accounts/{accountId}/devices',
				verb: 'GET'
			},
			'olegmonsterui.clicktocall.list': {
				url: 'accounts/{accountId}/clicktocall',
				verb: 'GET'
			},
			'olegmonsterui.clicktocall.create': {
				url: 'accounts/{accountId}/clicktocall',
				verb: 'PUT'
			}
		},

		// Define the events available for other apps 
		subscribe: {},

		// Method used by the Monster-UI Framework, shouldn't be touched unless you're doing some advanced kind of stuff!
		load: function(callback) {
			var self = this;

			self.initApp(function() {
				callback && callback(self);
			});
		},

		// Method used by the Monster-UI Framework, shouldn't be touched unless you're doing some advanced kind of stuff!
		initApp: function(callback) {
			var self = this;

			/* Used to init the auth token and account id of this app */
			monster.pub('auth.initApp', {
				app: self,
				callback: callback
			});
		},

		// Entry Point of the app
		render: function(container) {
			var self = this,
				parent = _.isEmpty(container) ? $('#monster-content') : container;
			
			self.getDeviceList(function(devices) {
				var testModuleTemplate = $(monster.template(self, 'testModule', {
					devices: devices
				}));
				
				self.bindEvents(testModuleTemplate);
				
				(parent).empty().append(testModuleTemplate);
			});
		},
		
		getDeviceList: function(callback) {
			var self = this;
		
			monster.request({
				resource: 'olegmonsterui.devices.list',
				data: {
					accountId: self.accountId
				},
				success: function(response) {
					var devices = response.data;
					
					callback && callback(devices);
				},
				error: function(response) {
					monster.ui.alert('Error getting device list.');
				}
			});
		},
		
		bindEvents: function(parent) {
			var self = this;
		
			parent.find('.device-item').on('click', function(e) {
				var $this = $(this),
					deviceId = $this.data('device-id'),
					deviceName = $this.data('device-name');

				self.getClickToCallList(function(clicktocalls) {
					var clickToCallWithDeviceId;
				
					$.each(clicktocalls, function(index, clicktocall) {
						if (clicktocall.extension === deviceId) {
							clickToCallWithDeviceId = clicktocall;
							return false;
						}
					});
					
					if (!clickToCallWithDeviceId) {
						self.createClickToCall({ 
							name: deviceName, 
							extension: deviceId,
							auth_required: false
						}, function(clicktocall) {
							self.showPhpCode(parent, clicktocall.id);
							self.setSelectedDevice(parent, $this);
						});
					} else {
						self.showPhpCode(parent, clickToCallWithDeviceId.id);
						self.setSelectedDevice(parent, $this);
					}
				});
			});
		},
		
		getClickToCallList: function(callback) {
			var self = this;
			
			monster.request({
				resource: 'olegmonsterui.clicktocall.list',
				data: {
					accountId: self.accountId
				},
				success: function(response) {
					var clicktocalls = response.data;
				
					callback && callback(clicktocalls);
				},
				error: function(response) {
					monster.ui.alert('Error getting click-to-call list.');
				}
			});
		},
		
		createClickToCall: function(dataForCreation, callback) {
			var self = this;
			
			monster.request({
				resource: 'olegmonsterui.clicktocall.create',
				data: {
					accountId: self.accountId,
					data: dataForCreation
				},
				success: function(response) {
					var clicktocall = response.data;
					
					callback && callback(clicktocall);
				},
				error: function(response) {
					monster.ui.alert('Error creating click-to-call.');
				}
			});
		},
		
		showPhpCode: function(parent, clickToCallId) {
			var self = this,
				activateUrl = self.apiUrl + 'accounts/' + self.accountId + '/clicktocall/' + clickToCallId + '/connect';
				
				phpCodeTemplate = $(monster.template(self, 'phpCode', {
					activateUrl: activateUrl
				}));

			parent.find('#php_code_container').empty().append(phpCodeTemplate);
		},
		
		setSelectedDevice: function(parent, deviceItem) {
			parent.find('.device-item').removeClass('active');
			deviceItem.addClass('active');
		}
	};

	return app;
});
